from PyQt5 import QtCore, QtWidgets
import MyWindow

class MyDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        self.myWidget = MyWindow.MyWindow()
        self.myWidget.vbox.setContentsMargins(0, 0, 0, 0)
        self.btn = QtWidgets.QPushButton("Cha&nge")
        mainBox = QtWidgets.QVBoxLayout()
        mainBox.addWidget(self.myWidget)
        mainBox.addWidget(self.btn)
        self.setLayout(mainBox)
        self.btn.clicked.connect(self.on_clicked)

    def on_clicked(self):
        self.myWidget.label.setText("New label")
        self.btn.setDisabled(True)

def main():
    import sys
    app = QtWidgets.QApplication(sys.argv)
    window = MyDialog()
    window.setWindowTitle("OOP")
    window.resize(300, 100)
    window.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()