from PyQt5 import QtWidgets, uic

class MyWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        uic.loadUi("gui/gui.ui", self)
        self.pushButton.clicked.connect(QtWidgets.qApp.quit)

def main():
    import sys
    app = QtWidgets.QApplication(sys.argv)
    window = MyWindow()
    window.show()
    sys.exit(app.exec_())
    print("after")

if __name__ == '__main__':
    main()