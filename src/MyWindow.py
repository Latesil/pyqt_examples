from PyQt5 import QtWidgets, QtCore

class MyWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.label = QtWidgets.QLabel("Hi there")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.btn = QtWidgets.QPushButton("&Close")
        self.vbox = QtWidgets.QVBoxLayout()
        self.vbox.addWidget(self.label)
        self.vbox.addWidget(self.btn)
        self.setLayout(self.vbox)
        self.btn.clicked.connect(QtWidgets.qApp.quit)

def main():
    import sys
    app = QtWidgets.QApplication(sys.argv)
    window = MyWindow()
    window.setWindowTitle("OOP")
    window.resize(300, 70)
    window.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()